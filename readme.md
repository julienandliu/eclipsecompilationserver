Setting Up
========================

## Author(s)
*   Romain Julien
*   Lisa Liu-Thorrold

--------------------------------------------------------------------------------
## Getting started
##### These instructions are for setup using the Eclipse IDE 
1. You will need to install the Eclipse Plugin Development Environment plugin (Eclipse PDE), so that you can work on Eclipse plugins. To to Help -> Eclipse MarketPlace -> type "Eclipse PDE" and download

2. Clone the repository from the following location

    https://lisa_liu-thorrold@bitbucket.org/julienandliu/eclipsecompilationserver.git
    
3. Import the project you have cloned into Eclipse as a normal or a git project
4. Now we need to make sure that we have Eclipse Java Development Tools installed. In Eclipse, go to Help -> Install new software and add type in the following link:
 	http://download.eclipse.org/releases/neon
Under the Programming Languages section, select Eclipse Java Development Tools and install

5. Everything should compile nicely now due to configurations being setup in plugin.xml and MANIFEST.MF

6. To run the plugin as a headless application, open plugin.xml
7. Go to the overview tab, and click on 'Export Wizard' under the 'Exporting' Section
8. Select the plugin that you have made, and put the path of the dropins director of your current Eclipse installation. For example, if it is in the Applications Folder on Mac, then it could be located at: 
`/Applications/Eclipse.app/Contents/Eclipse/dropins`
9. If your plugin has any errors, then the plugin will not export properly and throw an error. Finally we can execute the plugin from the command line. Navigate to the installation directory of your Eclipse innstallation. For example: `/Applications/Eclipse.app/Contents/MacOS` contains an Eclipse executable in Mac
10. Run the following command: `./eclipse -data "/Users/Lisa/Desktop/workspace" -nosplash -application EclipseCompilationServer.myapp`

> *  The **-nosplash** flag tells Eclipse to hide the splash screen, 
> *  the **-data** flag tells Eclipse which workspace to use. 
> *  The **-application** flag tells Eclipse to run a custom application.

 ** If you run into any problems, repeat everything on a fresh install of Eclipse. These instructions work for Eclipse neon version **

