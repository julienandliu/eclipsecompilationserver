package communication;

import java.util.List;

public class ProjectObject {
	private String name;
	private String path;
	private String type;
	private List<ProjectObject> children;
	
	public ProjectObject() {
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setChildren(List<ProjectObject> children) {
		this.children = children;
	}
	
	public List<ProjectObject> getChildren() {
		return children;
	}
	
	public String getName() {
		return name;
	}
	
	
}
