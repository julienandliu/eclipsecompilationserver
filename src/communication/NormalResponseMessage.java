package communication;

public class NormalResponseMessage extends ResponseMessage {
	
	// e.g File path returned if applicable
	private String responseContent;
	
	public NormalResponseMessage(String requestID, int responseCode, String responseComment) {
		this.requestID = requestID;
		this.responseCode = responseCode;
		this.responseComment = responseComment;
		this.responseContent = "";
	}
	
	public NormalResponseMessage(String requestID, int responseCode, String responseContent, String responseComment) {
		this.requestID = requestID;
		this.responseCode = responseCode;
		this.responseComment = responseComment;
		this.responseContent = responseContent;
	}
	
	
	public String getResponseContent() {
		return responseContent;
	}

}
