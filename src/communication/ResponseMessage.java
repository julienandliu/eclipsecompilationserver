package communication;

public abstract class ResponseMessage {
	
	/**
	 * 200 - OK
	 * 400 - NOT OK
	 */
	protected int responseCode;
	// e.g Comment for response code, or relevant error message.
	protected String responseComment;
	protected String requestID;
	
	public int getResponseCode() {
		return responseCode;
	}
	
	public String getResponseComment() {
		return responseComment;
	}
	
	public String getRequestID() {
		return requestID;
	}
	
}

