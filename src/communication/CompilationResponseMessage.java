package communication;

import eclipsecompilationserver.CompilationError;

public class CompilationResponseMessage extends ResponseMessage {
	
	private CompilationError[] compilationErrors;
	
	public CompilationResponseMessage(String requestID, int responseCode, String responseComment, CompilationError[] compilationErrors ) {
		this.responseCode = responseCode;
		
		//number of compilation errors
		this.responseComment = responseComment;
		this.compilationErrors = compilationErrors;
		this.requestID = requestID;
	}
	
	public CompilationError[] getCompilationErrors() {
		return compilationErrors;
	}
	
}
