package communication;

import java.util.Arrays;

public class CommandMessage {

	private String commandName;
	private String objectType;
	private String objectName;
	private String[] additionalParameters;
	private String requestID;
	
	/**
	 * Perhaps additional Parameters should be an array
	 * @param commandName
	 * @param objectType
	 * @param objectName
	 * @param additionalParameters
	 */
	public CommandMessage(String requestID, String commandName, String objectType, String objectName, String[] additionalParameters) {
		this.requestID = requestID;
		this.commandName = commandName;
		this.objectType = objectType;
		this.objectName = objectName;
		// collapse the white spaces between the comma
		this.additionalParameters = additionalParameters;
		
	}
	
	public void printMessage() {
		System.out.println("Command Name: " + commandName);
		System.out.println("Object Type: " + objectType);
		System.out.println("Object Name: " + objectName);
		System.out.println("Additional Parameters: " + Arrays.toString(additionalParameters));
	}
	
	public String getCommand() {
		return commandName;
	}
	
	public String getObjectType() {
		return objectType;
	}
	
	public String getObjectName() {
		return objectName;
	}
	
	public String[] getAdditionalParameters() {
		return additionalParameters;
	}
	
	public String getRequestID() {
		return requestID;
	}
	
	
}
