package communication;

public class ProjectStructureMessage extends ResponseMessage {
	
	private ProjectObject projectStructure;

	public ProjectStructureMessage(String requestID, int responseCode, String responseComment, ProjectObject po) {
		this.requestID = requestID;
		this.responseComment = responseComment;
		this.projectStructure = po;
		this.responseCode = responseCode;
	}

	public ProjectStructureMessage(String requestID, int responseCode, ProjectObject po) {
		this.requestID = requestID;
		this.projectStructure = po;
		this.responseCode = responseCode;
	}


	public ProjectObject getProjectObject() {
		return projectStructure;
	}

}
