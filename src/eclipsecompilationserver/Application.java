package eclipsecompilationserver;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.launching.LibraryLocation;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;

import com.google.gson.Gson;

import communication.CommandMessage;
import communication.CompilationResponseMessage;
import communication.NormalResponseMessage;
import communication.ProjectObject;
import communication.ProjectStructureMessage;

/**
 * This class controls all aspects of the application's execution
 */
public class Application implements IApplication {

	private IProgressMonitor progressMonitor = new NullProgressMonitor();	//default iprogressmonitor implementation
	private IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
	private Gson gson = new Gson();
	private Thread processMessageThread;
	private PrintWriter out;

	private LinkedBlockingQueue<String> inboundRequestQueue;

	@SuppressWarnings("resource")
	public Object start(IApplicationContext context) throws Exception {

		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(9000);
			System.out.println("Server is activated, listening on port 9000");
			inboundRequestQueue = new LinkedBlockingQueue<>();
			processMessageThread = new Thread(new ClientRequest(inboundRequestQueue));
			processMessageThread.start();

			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					try {
						ResourcesPlugin.getWorkspace().save(false,null);
					} catch (CoreException e) {
						e.printStackTrace();
					}
				}
			});


		} catch (Exception e) {
			e.printStackTrace();
		}

		Socket connSocket = null;

		while(true) {
			try {
				connSocket = serverSocket.accept();
				System.out.println("Connection accepted from: " + connSocket.getInetAddress().getHostAddress());

			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				// Now have a socket to use for communication
				// Create a PrintWriter and BufferedReader for interaction with our stream "true" means we flush the stream on newline
				out = new PrintWriter(connSocket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(connSocket.getInputStream()));
				String line;
				// Read a line from the stream - until the stream closes
				while ((line=in.readLine()) != null) {
					System.out.println("Message from client: " + line);
					inboundRequestQueue.add(line);
				}

				System.out.println("Client " + connSocket.getInetAddress().getHostName() +" finish up");
				out.close();
				in.close();
				connSocket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}


	private void processMessage(String message) throws Exception {
		
		String response = "";

		try {
			CommandMessage msgObject = gson.fromJson(message, CommandMessage.class);

			System.out.println("Message: " + message);

			switch (msgObject.getCommand()) {
			case "create":
				response = createObject(msgObject);
				break;
			case "delete":
				response = deleteObject(msgObject);
				break;
			case "change":
				response = changeObject(msgObject);
				break;
			case "compile":
				response = compile(msgObject);
				break;
			case "retrieve":
				response = retrieveObject(msgObject);
				break;
			}

		} catch (Exception e) {
			String responseComment = "Error processing request - message format incorrect. Check wiki for correct emssage format";
			response = gson.toJson(new NormalResponseMessage("unknown - crashed parsing JSON so requestID is unknown", 400, responseComment));
		}

		System.out.println(response);
		out.println(response); 
	}

	private String compile(CommandMessage msgObject) throws Exception {
		return buildProject(msgObject.getRequestID(), msgObject.getObjectName());
	}

	private String changeObject(CommandMessage msgObject) throws Exception {

		String requestID = msgObject.getRequestID();
		String objectName = msgObject.getObjectName();
		String[] additionalParameters = msgObject.getAdditionalParameters();

		switch (msgObject.getObjectType()) {
		case "project":
			return changeProject(requestID, objectName, additionalParameters);
		case "package":
			return changePackage(requestID, objectName, additionalParameters);
		case "class":
			return changeClass(requestID, objectName, additionalParameters);
		case "folder":
			return changeFolder(requestID, objectName, additionalParameters);
		case "file":
			return changeFile(requestID, objectName, additionalParameters);

		default:
			String responseComment = "Error processing request";
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));
		}
	}

	private String retrieveObject(CommandMessage msgObject) throws Exception {

		String requestID = msgObject.getRequestID();
		String objectName = msgObject.getObjectName();
		String[] additionalParameters = msgObject.getAdditionalParameters();

		switch (msgObject.getObjectType()) {
		case "project":
			return	retrieveProject(requestID, objectName, additionalParameters);

		default:
			String responseComment = "Error processing request";
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));
		}
	}

	private String retrieveProject(String requestID, String projectName, String[] additionalParams) {
		switch (additionalParams[0]) {
		case "structure":
			return retrieveProjectStructure(requestID, projectName);

		default:
			String responseComment = "Error processing request";
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));
		}
	}	


	/** User wants to change the project. The possible things that can be changed at the project level include
	 * 1. Project Name - not sure if this is required because this will be managed by node.js (i.e It will map the
	 * 					 project name to an ID to make on the Eclipse plugin backend, so that we can have multiple 
	 * 					 projects with the same name.
	 * 
	 * @param projectName - The project to change
	 * @param additionalParams - The first item in the array should be the type of property in the project to change
	 * 						   - The second/ consecutive items in the array should be the new value of the property
	 * @return JSON formatted string for the response of whether the change was done successfully
	 * @throws CoreException 
	 */
	private String changeProject(String requestID, String projectName, String[] additionalParams) throws CoreException {
		switch (additionalParams[0]) {
		case "name":
			return changeProjectName(requestID, projectName, additionalParams[1]);
		default:
			String responseComment = "Error processing request";
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));
		}
	}

	/**
	 * This changes the name of the project. If the current project to rename does not exist, it returns a 400 error.
	 * @param currentProjectName 
	 * @param newProjectName 
	 * @return response to client in JSON 200/400
	 * @throws CoreException
	 * 
	 * - Tested (changing it to the same name does not throw an error)
	 */
	private String changeProjectName(String requestID, String currentProjectName, String newProjectName) throws CoreException {

		IProject project = root.getProject(currentProjectName);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);
		NormalResponseMessage response;
		String responseComment;

		if (project.exists()) {

			try {
				project.open(progressMonitor);
				IProjectDescription desc = project.getDescription();
				desc.setName(newProjectName);
				project.move(desc, true, null);

				// confirm new project name:
				IProject newProject = root.getProject(newProjectName);

				String newProjectPath = root.getRawLocation().toString() + newProject.getFullPath();
				responseComment = "Project renamed from: " + currentProjectName + " to: " + 
						newProject.getDescription().getName();

				response = new NormalResponseMessage(requestID, 200, newProjectPath, responseComment);
			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error processing this request. Check the console log for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}

		} else {
			responseComment = "Error renaming project - project doesn't exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);
	}

	/**
	 * User wants to change the package. This possible things that can be changed at the package level include
	 * 1. Package Name - Should update all the files in this package with the new package name
	 * @param packageName - The name of the current package to modify
	 * @param additionalParams - The first item in the array should be the type of property in the package to change
	 *  					   - The second/ consecutive items in the array should be the new value of the property
	 * @return JSON formatted string for the response of whether the change was done successfully
	 * @throws CoreException 
	 */
	private String changePackage(String requestID, String packageName, String[] additionalParams) throws CoreException {
		switch (additionalParams[0]) {
		case "name":
			return changePackageName(requestID, packageName, additionalParams[1], additionalParams[2]);

			// should never reach here
		default: 
			String responseComment = "Error processing request";
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));
		}
	}

	/**
	 * Changes the name of a package for a given project. This method returns an error when the current package name 
	 * and the new package name is the same, or if the project does not exist, or is invalid. If the new package name 
	 * already exists, then everything from the current package will be moved to this new package (unless there is a
	 * collision with the classes being moved to the new package), and the old package deleted. This method will update 
	 * the package name within the class automatically. 
	 * - Tested
	 * @param currentPackageName - The current package name to change the name of
	 * @param newPackageName - The new name of the package
	 * @param projectName - The project that this package belongs to
	 * @return response to client in JSON. 200/400.
	 * @throws CoreException 
	 */ 
	private String changePackageName(String requestID, String currentPackageName, String newPackageName, String projectName) throws CoreException {

		String responseComment;

		// Check curr package name & new package name are different, otherwise a big error will be thrown when trying
		// to perform rename operation
		if (currentPackageName.equals(newPackageName)) {
			responseComment = "New package name must be different to the current package name";
			return gson.toJson(new NormalResponseMessage(requestID, 400, 
					responseComment));
		}

		NormalResponseMessage response;

		// Get the project handler
		IProject project = root.getProject(projectName);

		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if (javaProject.exists()) {
			try {
				IFolder sourceFolder = project.getFolder("src");
				IPackageFragment pack = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(currentPackageName);
				pack.rename(newPackageName, false, progressMonitor);

				// delete old package recursively if necessary
				String[] arr = currentPackageName.split("\\.");
				IPackageFragment oldPackage = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(currentPackageName);
				int length = arr.length;

				while (length > 0) {

					String temp = "";

					if (length == 1) {
						temp = arr[0];
					} else {

						for (int i=0; i < length -1; i++) {
							temp += arr[i] + ".";
						}

						temp += arr[length-1];
					}


					oldPackage = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(temp);

					if ((oldPackage.exists()) && (oldPackage.hasSubpackages() || oldPackage.containsJavaResources())) {
						break;
					} else {
						if (oldPackage.exists()) {
							oldPackage.delete(false, null);
							System.out.println("Package " + pack.getElementName() + " deleted.");
						}
						length --;
					}
				}

				pack = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(newPackageName);

				// Send response back to client
				String newPackagePath = root.getRawLocation().toString() + pack.getPath();
				responseComment = "Package " +  currentPackageName + 
						" successfully renamed to " + newPackageName + " in project " + projectName;
				response = new NormalResponseMessage(requestID, 200, newPackagePath, responseComment);


			} catch (Exception e) {
				responseComment = "Error. Unable to rename package. Check command, or change package name.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
				e.printStackTrace();
			}

		} else {
			responseComment = "Error creating package. Project does not exist, or is invalid";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);

	}

	/**
	 * User wants to change the class. The possible things that can be changed at the class level include
	 * 1. Class Name - Should update all the files in this package with the new class name
	 * 2. Package of the class
	 * @param className - The name of the current class to modify
	 * @param additionalParams - The first item in the array should be the type of property in the class to change
	 * 						   - The second/ consecutive items in the array should be the new value of the propert
	 * @return JSON formatted string for the response of whether the change was done successfully
	 * @throws CoreException 
	 */
	private String changeClass(String requestID, String className, String[] additionalParams) throws CoreException {
		switch (additionalParams[0]) {
		case "name":
			// old class name, new class name, project name, packageName
			return changeClassName(requestID, className, additionalParams[1], additionalParams[2], additionalParams[3]);
		case "package":
			return changeClassPackage(requestID, className, additionalParams[1], additionalParams[2], additionalParams[3]);
		default:
			String responseComment = "Error processing request";
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));

		}
	}

	/**
	 * User wants to change the folder. The possible things that can be changed on the folder include
	 * 1. Folder Name
	 * 2. Folder parent (i.e. Relocating the folder)
	 * @param requestID
	 * @param folderName
	 * @param additionalParams additionalParams[0] should be parameter user wants to change, 
	 * 		  additionalParams[1] should be project name
	 * 		  additionalParams[2] current parent folder
	 * 		  additionalParams[3] should be the new folder name (or new parent folder)
	 * @return JSON formatted response indicating the success of the request
	 * @throws Exception
	 */
	private String changeFolder(String requestID, String folderName, String[] additionalParams) throws Exception {

		String projectName = additionalParams[1];
		String currentParentFolder = additionalParams[2];
		String newValue = additionalParams[3];

		switch (additionalParams[0]) {
		case "name":
			return changeFolderName(requestID, folderName, projectName, currentParentFolder, newValue);
		case "parentFolder":
			return changeFolderParent(requestID, folderName, projectName, currentParentFolder, newValue);

		default:
			String responseComment = "Error processing request";
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));

		}
	}

	/**
	 * User wants to change the file. The possible things that can be changed on the file include
	 * 1. File Name
	 * 2. File parent (i.e Relocating the file)
	 * @param requestID
	 * @param fileName
	 * @param additionalParams additionalParams[0] should be parameter user wants to change, 
	 * 		  additionalParams[1] should be project name
	 * 		  additionalParams[2] current parent folder
	 * 		  additionalParams[3] should be the new file name (or new parent folder)
	 * @return JSON formatted response indicating the success of the request
	 * @throws Exception
	 */
	private String changeFile(String requestID, String fileName, String[] additionalParams) throws Exception {

		String projectName = additionalParams[1];
		String currentParentFolder = additionalParams[2];
		String newValue = additionalParams[3];

		switch (additionalParams[0]) {
		case "name":
			return changeFileName(requestID, fileName, projectName, currentParentFolder, newValue);
		case "parentFolder":
			return changeFileParent(requestID, fileName, projectName, currentParentFolder, newValue);

		default:
			String responseComment = "Error processing request";
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));

		}
	}

	/**
	 * This method gets invoked when the user wants to change the parent folder of the file (e.g. Move the file into
	 * another parent folder
	 * 
	 * @param requestID
	 * @param fileName - The file to move
	 * @param projectName - The project the file is located in
	 * @param currParentFolder 
	 * @param newParentFolder
	 * @return JSON formatted response indicating the success of the request
	 * @throws Exception
	 * 
	 * -Tested
	 */
	private String changeFileParent(String requestID, String fileName, String projectName, String currParentFolder,
			String newParentFolder) throws Exception {

		NormalResponseMessage response;
		String responseComment;

		// Get the project handler
		IProject project = root.getProject(projectName);

		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if (javaProject.exists()) {

			try {

				project.open(progressMonitor);

				String fullPath = project.getFullPath() + "/" + currParentFolder + fileName;
				IFile file = root.getFile(new Path(fullPath));

				String newPath = project.getFullPath() + "/" + newParentFolder + fileName;
				String newFullPath = root.getRawLocation().toString() + newPath;

				if (file.exists()) {

					file.copy(new Path(newPath), false, null);
					file.delete(false, true, null);

					responseComment = "File " + fileName + " moved from " + currParentFolder + " to " + 
							newParentFolder + " in project " + projectName;
					response = new NormalResponseMessage(requestID, 200, newFullPath, responseComment);
				} else {
					responseComment = "File does not exist at the specified location. Can not change parent folder (move).";
					response = new NormalResponseMessage(requestID, 400, responseComment);
				}

				// NEED TO SAVE!!
				ResourcesPlugin.getWorkspace().save(true, null);
			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error processing this request. Check the console log for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}


		} else {
			//project does not exist
			responseComment = "Project does not exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);
	}

	/**
	 * User wants to change the name of this file
	 * 
	 * @param requestID
	 * @param fileName - name of the file user wants to change
	 * @param projectName - project that the file is currently located in
	 * @param parentFolder 
	 * @param newFileName
	 * @return JSON formatted response 
	 * @throws Exception
	 * 
	 * - Tested
	 */
	private String changeFileName(String requestID, String fileName, String projectName, String parentFolder,
			String newFileName) throws Exception {

		NormalResponseMessage response;
		String responseComment;

		// Get the project handler
		IProject project = root.getProject(projectName);
		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if (javaProject.exists()) {

			try {
				project.open(progressMonitor);

				String fullPath = project.getFullPath() + "/" + parentFolder + fileName;
				IFile file = root.getFile(new Path(fullPath));
				String newPath = project.getFullPath() + "/" + parentFolder + newFileName;
				String newFullPath = root.getRawLocation().toString() + newPath;

				if (file.exists()) {

					file.copy(new Path(newPath), false, null);
					file.delete(false, true, null);

					responseComment = "File " + fileName + " renamed to " + newFileName + " in project " + projectName;
					response = new NormalResponseMessage(requestID, 200, newFullPath, responseComment);
				} else {
					responseComment = "File does not exist at the specified location. Can not rename.";
					response = new NormalResponseMessage(requestID, 400, responseComment);
				}

				// NEED TO SAVE!!
				ResourcesPlugin.getWorkspace().save(true, null);
			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error processing this request. Check the console log for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}


		} else {
			//project does not exist
			responseComment = "Project does not exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);

	}


	/**
	 * User wants to change the parent of this folder
	 * @param requestID
	 * @param folderName - The folder to move/change parent
	 * @param projectName - The project which the folder is located in
	 * @param currParentFolder
	 * @param newParentFolder
	 * @return JSON formatted response indicating the success of the request
	 * @throws Exception
	 * 
	 * -Tested
	 */
	private String changeFolderParent(String requestID, String folderName, String projectName, String currParentFolder, 
			String newParentFolder) throws Exception {

		NormalResponseMessage response;
		String responseComment;

		// Get the project handler
		IProject project = root.getProject(projectName);

		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);
		if (javaProject.exists()) {

			try {

				project.open(progressMonitor);
				IFolder folderToMove = project.getFolder(currParentFolder + folderName);

				IFolder newFolder = project.getFolder(newParentFolder + folderName);
				folderToMove.copy(newFolder.getFullPath(), false, null);
				folderToMove.delete(false, true, null);

				if (newParentFolder.isEmpty()) {
					newParentFolder = "/";
				}

				String path = root.getRawLocation().toString() + newFolder.getFullPath().toString();
				responseComment = "Folder " + folderName + " moved from " + currParentFolder + " to " + newParentFolder 
						+ " in project " + projectName;
				response = new NormalResponseMessage(requestID, 200, path,  responseComment);

			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error processing this request. Check the console log for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}

		} else {
			//project does not exist
			responseComment = "Project does not exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);

	}

	/**
	 * Invoking this method will rename a folder. It will do this by creating a new empty folder with the desired name,
	 * and moving everything across
	 * @param requestID
	 * @param folderName - current folder name
	 * @param projectName - project that the folder is located in
	 * @param parentFolder - the path of the parent folder
	 * @param newFolderName - the name of the new folder
	 * @return JSON formatted string indicating the success of the request
	 * @throws Exception
	 * 
	 * - Tested
	 */
	private String changeFolderName(String requestID, String folderName, String projectName, 
			String parentFolder, String newFolderName) throws Exception {

		NormalResponseMessage response;
		String responseComment;

		// Get the project handler
		IProject project = root.getProject(projectName);


		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);
		if (javaProject.exists()) {

			try {

				project.open(progressMonitor);
				IFolder folderToRename = project.getFolder(parentFolder + folderName);

				IFolder newFolder = project.getFolder(parentFolder + newFolderName);
				folderToRename.copy(newFolder.getFullPath(), false, null);
				folderToRename.delete(false, true, null);

				String path = root.getRawLocation().toString() + newFolder.getFullPath().toString();
				responseComment = "Folder " + folderName + " renamed to " + newFolderName + " in project " + projectName;
				response = new NormalResponseMessage(requestID, 200, path,  responseComment);

			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error processing this request. Check the console log for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}

		} else {
			//project does not exist
			responseComment = "Project does not exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);

	}

	/**
	 * The client would like to change the name of the class
	 * @param oldClassName
	 * @param newClassName
	 * @param projectName
	 * @param packageName
	 * @return JSON response message to indicate whether the change was successful or not
	 * 
	 * - Tested. Class name inside the file gets updated. 
	 * @throws CoreException 
	 */
	private String changeClassName(String requestID, String oldClassName, String newClassName, String packageName, String projectName) throws CoreException {

		String responseComment;

		// Check curr package name & new package name are different, otherwise a big error will be thrown when trying
		// to perform rename operation
		if (oldClassName.equals(newClassName)) {
			responseComment = "New class name must be different to the current class name";
			return gson.toJson(new NormalResponseMessage(requestID, 400, 
					responseComment));
		}

		NormalResponseMessage response;

		IProject project = root.getProject(projectName);
		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if(!project.exists() || !javaProject.exists()) {
			responseComment = "Error changing class name. Project does not exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
			return gson.toJson(response);
		}

		IFolder sourceFolder = project.getFolder("src");

		if (!oldClassName.contains(".java")) {
			oldClassName = oldClassName + ".java";
		}

		if (!newClassName.contains(".java")) {
			newClassName = newClassName + ".java";
		}

		try { 
			// Get the package containing the class
			IPackageFragment pack = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(packageName);
			pack.getCompilationUnit(oldClassName).rename(newClassName, false, progressMonitor);

			ICompilationUnit newClass = pack.getCompilationUnit(newClassName);

			String newClassPath = root.getRawLocation().toString() + newClass.getPath();
			responseComment = "Class " + oldClassName + 
					" renamed successfully to " + newClassName + " in package " + packageName;

			response = new NormalResponseMessage(requestID, 200, newClassPath, responseComment);
		} catch (Exception e) {
			responseComment = "Error renaming class " + oldClassName + " - naming collision. Check console log for stack trace.";
			response = new NormalResponseMessage(requestID, 400, responseComment);
			e.printStackTrace();
		}

		return gson.toJson(response);

	}

	/**
	 * The user would like to change the package that a class belongs to
	 * @param className
	 * @param oldPackageName
	 * @param newPackageName
	 * @param projectName
	 * @return JSON response indicating success of class package change
	 * 
	 * - Tested. Package declaration inside class file is automatically renamed.
	 * - Assumes the new package exists.
	 * @throws CoreException 
	 */
	private String changeClassPackage(String requestID, String className, String oldPackageName, String newPackageName, String projectName) throws CoreException {

		NormalResponseMessage response;
		String responseComment =  "Error changing class package. Project does not exist";

		IProject project = root.getProject(projectName);
		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if(!project.exists() || !javaProject.exists()) {
			response = new NormalResponseMessage(requestID, 400, responseComment);
			return gson.toJson(response);
		}


		IFolder sourceFolder = project.getFolder("src");

		if (!className.contains(".java")) {
			className = className + ".java";
		}

		// Get the package containing the class
		IPackageFragment pack = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(oldPackageName);

		try {		
			// will throw error if class cannot be found within this package
			ICompilationUnit cu = pack.getCompilationUnit(className);
			IPackageFragment newPack = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(newPackageName);

			if (newPack.exists()) {
				cu.move(newPack, null, null, false, progressMonitor);


				ICompilationUnit theClass = newPack.getCompilationUnit(className);

				String newClassPath = root.getRawLocation().toString() + theClass.getPath();
				responseComment = "Class " + className + 
						" moved successfully from package " + oldPackageName + " to package " + newPackageName;

				response = new NormalResponseMessage(requestID, 200, newClassPath, responseComment);

			} else {
				responseComment = "Error moving class package - new package does not exist";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}

		} catch (Exception e) {
			responseComment = "Error moving class package - naming collision. Check console log for stack trace.";
			response = new NormalResponseMessage(requestID, 400, responseComment);
			e.printStackTrace();
		}


		return gson.toJson(response);
	}

	private String deleteObject(CommandMessage msgObject) throws Exception {

		String requestID = msgObject.getRequestID();
		String objectName = msgObject.getObjectName();
		String[] additionalParams = msgObject.getAdditionalParameters();

		switch(msgObject.getObjectType()) {
		case "project":
			return deleteProject(requestID, objectName);
		case "package":
			return deletePackage(requestID, objectName, additionalParams);
		case "class":
			return deleteClass(requestID, objectName, additionalParams);
		case "folder":
			return deleteFolder(requestID, objectName, additionalParams);
		case "file":
			return deleteFile(requestID, objectName, additionalParams);

			//shouldn't reach here
		default:
			String responseComment = "Error processing request";
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));
		}
	}


	private String createObject(CommandMessage msgObject) throws Exception {

		String requestID = msgObject.getRequestID();
		String objectName = msgObject.getObjectName();
		String[] additionalParams = msgObject.getAdditionalParameters();

		switch(msgObject.getObjectType()) {
		case "project":
			return createProject(requestID, objectName);
		case "package":
			return createPackage(requestID, objectName, additionalParams);
		case "class":
			return createClass(requestID, objectName, additionalParams);
		case "folder":
			return createFolder(requestID, objectName, additionalParams);
		case "file":
			return createFile(requestID, objectName, additionalParams);
		default:
			// shouldn't reach here
			String responseComment = "Error processing request";
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));
		}	
	}


	/**
	 * Invoking this method will delete the specified package for a given project. This method will assume that there
	 * are no files currently in that package, otherwise it will return an error, and not perform the operation.
	 * @param packageName
	 * @param additionalParams
	 * @return JSON formatted message indicating success of client's request
	 * @throws Exception
	 * 
	 * - Tested
	 */
	private String deletePackage(String requestID, String packageName, String[] additionalParams) throws Exception {

		// Get the project
		if (additionalParams.length != 1) {
			return gson.toJson(new NormalResponseMessage(requestID, 400, "Malformed Message"));
		} 

		return deletePackage(requestID, packageName, additionalParams[0]);

	}


	/**
	 * Invoking this method will create a package for a given project. This method assumes that the package name
	 * specified does not already exist in the project. This method will throw an error if either 
	 * 1. The package already exists
	 * 2. The project cannot be found/invalid
	 * @param packageName 
	 * @param additionalParams - The first element in additionalParams is the name of the project
	 * @return JSON formatted response indicating success of client's request
	 * @throws Exception
	 */
	private String createPackage(String requestID, String packageName, String[] additionalParams) throws Exception {

		if (additionalParams.length != 1) {
			return gson.toJson(new NormalResponseMessage(requestID, 400, "Malformed Message"));
		}

		return createPackage(requestID, packageName, additionalParams[0]);

	}

	// Additional params should contain 1. project name 2. package name (if not supplied, then use default)

	/**
	 * Invoking this method will delete a class from a given package and class. If the package name is not supplied,
	 * then it is assumed the user is deleting from the default package.
	 * @param className - The class to delete
	 * @param additionalParams - additionalParams[0] should contain the project name, additionalParams[1] should 
	 * 							 contain the package name.
	 * @return JSON formatted response indicating success of client's request
	 * @throws Exception
	 */
	private String deleteClass(String requestID, String className, String[] additionalParams) throws Exception {

		//only if project name supplied
		if (additionalParams.length == 1) {
			return deleteClass(requestID, additionalParams[0], "", className);
		}

		// project name and package name supplied
		if (additionalParams.length == 2) {
			return deleteClass(requestID, additionalParams[0], additionalParams[1], className);
		}

		// should not get here
		String responseComment = "Malformed Message";
		return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));

	}

	/**
	 * Invoking this method will create a file in the project and parent folder specified.
	 * @param requestID
	 * @param fileName
	 * @param additionalParams [projectName, parentFolder]
	 * @return JSON formatted response indicating the success of the requets
	 * @throws Exception
	 * 
	 * -Tested
	 */
	private String createFile(String requestID, String fileName, String[] additionalParams) throws Exception {
		String projectName = additionalParams[0];
		String parentFolder = additionalParams[1];

		NormalResponseMessage response;
		String responseComment;

		// Get the project handler
		IProject project = root.getProject(projectName);
		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if (javaProject.exists()) {

			try {
				project.open(progressMonitor);

				String fullPath = project.getFullPath() + "/" + parentFolder + fileName;
				IFile file = root.getFile(new Path(fullPath));

				if (file.exists()) {
					responseComment = "File exists at the desired location.";
					response = new NormalResponseMessage(requestID, 400, responseComment);
					return gson.toJson(response);
				}

				byte[] bytes = "".getBytes();
				InputStream source = new ByteArrayInputStream(bytes);
				file.create(source, IResource.NONE, null);

				responseComment = "File " + fileName + " created in project " + projectName;
				String path = root.getRawLocation().toString() + file.getFullPath().toString();
				response = new NormalResponseMessage(requestID, 200, path, responseComment);

				// NEED TO SAVE!!
				ResourcesPlugin.getWorkspace().save(true, null);


			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error processing this request. Check the console log for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}

		} else {
			//project does not exist
			responseComment = "Project does not exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);
	}

	/**
	 * Invoking this method will create folder in a project and parent folder specified.
	 * @param requestID
	 * @param folderName
	 * @param additionalParams  [projectName, parentFolder]
	 * @return JSON formatted responses indicating success of the request
	 * @throws Exception
	 * 
	 * - Tested
	 */
	private String createFolder(String requestID, String folderName, String[] additionalParams) throws Exception {

		String projectName = additionalParams[0];
		String parentFolder = additionalParams[1];

		NormalResponseMessage response;
		String responseComment;

		// Get the project handler
		IProject project = root.getProject(projectName);
		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if (javaProject.exists()) {

			try {
				project.open(progressMonitor);

				IFolder newFolder = project.getFolder(parentFolder + folderName);
				newFolder.create(false, true, null);

				responseComment = "Folder " + folderName + " created in project " + projectName;
				String path = root.getRawLocation().toString() + newFolder.getFullPath().toString();
				response = new NormalResponseMessage(requestID, 200, path, responseComment);

			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error processing this request. Check the console log for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}

		} else {
			//project does not exist
			responseComment = "Project does not exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);

	}

	// Additional params should contain 1. project name 2. package name (if not supplied, then use default package)
	private String createClass(String requestID, String className, String[] additionalParams) throws Exception {

		// only project name supplied
		if (additionalParams.length == 1) {
			return createClass(requestID, additionalParams[0], "", className);
		}

		// project name and package name supplied
		if (additionalParams.length == 2) {
			return createClass(requestID, additionalParams[0], additionalParams[1], className);
		}

		// should not get here
		String responseComment = "Malformed Message";
		return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));
	}

	/**
	 * Invoking this method will delete the project specified by the user. This method will throw an error if the
	 * project does not exist
	 * @param projectName - The project to delete
	 * @return JSON formatted response on whether the project was deleted successfully.
	 * @throws Exception
	 * 
	 * -Tested
	 */
	private String deleteProject(String requestID, String projectName) throws Exception {

		String responseComment;

		IProject project = root.getProject(projectName);
		NormalResponseMessage response;
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if (!project.exists()) {
			responseComment = "Project " + projectName + " does not exist";
			response = new NormalResponseMessage(requestID, 200, responseComment);
		} else {
			try {
				project.close(progressMonitor);
				project.delete(true, true, progressMonitor);
				responseComment = "Project " + projectName + " deleted";
				response = new NormalResponseMessage(requestID, 200, responseComment);
			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error processing this request. Check the console log for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}
		}

		return gson.toJson(response);
	}

	/**
	 * Invoking this method creates a new project in the workspace by the supplied name. If the project already exists,
	 * an error will be returned, and the project will not be made.
	 * @param projectName
	 * @return
	 * @throws Exception
	 */
	private String createProject(String requestID, String projectName) throws Exception {
		// Note: This method deals exclusively with resource handles, independent of whether the resources exist in the workspace. 
		// With the exception of validating that the name is a valid path segment, validation checking of the project name is not 
		// done when the project handle is constructed; rather, it is done automatically as the project is created. 
		IProject project = root.getProject(projectName);
		NormalResponseMessage response;
		String responseComment;
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		// check if workspace has the project already. if it does, return the path of the project
		//	root.
		if (project.exists()) {
			responseComment = "Project Exists";
			response = new NormalResponseMessage(requestID, 400, new String(root.getRawLocation().toString() + 
					project.getFullPath()), responseComment);
			return gson.toJson(response);

		} else {
			try {
				project.create(progressMonitor);
				project.open(progressMonitor);

				IProjectDescription description = project.getDescription();
				description.setNatureIds(new String[] {
						JavaCore.NATURE_ID});		//users must call IProject.setDescription(IProjectDescription, int, IProgressMonitor) before changes to this descrption take effect
				project.setDescription(description, progressMonitor);		

				// Finally create Java Project
				IJavaProject javaProject = JavaCore.create(project); //JavaCore gets singleton JavaModel to create a new JavaProject

				IFolder binFolder = project.getFolder("bin");
				binFolder.create(false, true, null);
				javaProject.setOutputLocation(binFolder.getFullPath(), null);

				ArrayList<IClasspathEntry> entries = new ArrayList<IClasspathEntry>();
				IVMInstall vmInstall = JavaRuntime.getDefaultVMInstall();
				LibraryLocation[] locations = JavaRuntime.getLibraryLocations(vmInstall);
				for (LibraryLocation element : locations) {
					entries.add(JavaCore.newLibraryEntry(element.getSystemLibraryPath(), null, null));
				}
				//add libs to project class path
				javaProject.setRawClasspath(entries.toArray(new IClasspathEntry[entries.size()]), null);

				IFolder sourceFolder = project.getFolder("src");
				sourceFolder.create(false, true, null);

				IPackageFragmentRoot packageFragmentRoot = javaProject.getPackageFragmentRoot(sourceFolder);
				IClasspathEntry[] oldEntries = javaProject.getRawClasspath();
				IClasspathEntry[] newEntries = new IClasspathEntry[oldEntries.length + 1];
				System.arraycopy(oldEntries, 0, newEntries, 0, oldEntries.length);
				newEntries[oldEntries.length] = JavaCore.newSourceEntry(packageFragmentRoot.getPath());
				javaProject.setRawClasspath(newEntries, null);

				//set the build path
				IClasspathEntry[] buildPath = {
						JavaCore.newSourceEntry(project.getFullPath().append("src")),
						JavaRuntime.getDefaultJREContainerEntry() };

				javaProject.setRawClasspath(buildPath, project.getFullPath().append(
						"bin"), null);


				String projectPath = root.getRawLocation().toString() + project.getFullPath();
				responseComment = "Project Created Successfully";
				response = new NormalResponseMessage(requestID, 200, projectPath, responseComment);
			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error processing this request. Check the console log for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}

		}

		return gson.toJson(response);
	}


	/**
	 * @param projectName
	 * @param packageName
	 * @param className
	 * @return
	 * @throws Exception
	 * 
	 * - Tested deleting from 1. A specific package 2. A package that doesn't exist 3. A blank package
	 */
	private String deleteClass(String requestID, String projectName, String packageName, String className) throws Exception {

		NormalResponseMessage response;
		String responseComment;

		IProject project = root.getProject(projectName);
		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if(!project.exists() || !javaProject.exists()) {
			responseComment = "Error deleting class. Project does not exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
			return gson.toJson(response);
		}


		IFolder sourceFolder = project.getFolder("src");

		if (!className.contains(".java")) {
			className = className + ".java";
		}

		try { 
			// Get the package containing the class
			IPackageFragment pack = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(packageName);
			pack.getCompilationUnit(className).delete(true, progressMonitor);
			responseComment = "Class " + className + " deleted successfully";
			response = new NormalResponseMessage(requestID, 200, responseComment);
		} catch (Exception e) {
			responseComment = "Unknown error deleting class: " + className;
			response = new NormalResponseMessage(requestID, 400, responseComment);

		}

		return gson.toJson(response);
	}

	/**
	 * Invoking this method will delete a file from the specified project and parent folder
	 * @param requestID
	 * @param fileName - The filename to delete
	 * @param additionalParams [projectName, parentFolder]
	 * @return
	 * @throws Exception
	 */
	private String deleteFile(String requestID, String fileName, String[] additionalParams) throws Exception {

		NormalResponseMessage response;
		String responseComment;
		String projectName = additionalParams[0];
		String parentFolder = additionalParams[1];


		IProject project = root.getProject(projectName);
		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if(!project.exists() || !javaProject.exists()) {
			responseComment = "Error deleting class. Project does not exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
			return gson.toJson(response);
		}

		project.open(progressMonitor);

		String fullPath = project.getFullPath() + "/" + parentFolder + fileName;
		IFile file = root.getFile(new Path(fullPath));
		System.out.println(fullPath);

		if (file.exists()) {

			file.delete(false, true, null);

			responseComment = "File " + fileName + " deleted from project " + projectName;
			response = new NormalResponseMessage(requestID, 200, responseComment);

		} else {
			responseComment = "File " + fileName + " does not exist in project " + projectName + " at the specified location";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}


		ResourcesPlugin.getWorkspace().save(true, null);
		return gson.toJson(response);
	}




	/**
	 * Invoking this method will delete a folder under a given project.
	 * @param requestID
	 * @param folderName - The folderName to delete
	 * @param additionalParams [projectName, parentFolder]
	 * @return
	 * @throws Exception 
	 * 
	 * - Tested
	 */
	private String deleteFolder(String requestID, String folderName, String[] additionalParams) throws Exception {
		String projectName = additionalParams[0];
		String parentFolder = additionalParams[1];

		NormalResponseMessage response;
		String responseComment;

		// Get the project handler
		IProject project = root.getProject(projectName);


		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);
		if (javaProject.exists()) {

			try {
				project.open(progressMonitor);

				IFolder folderToDelete = project.getFolder(parentFolder + folderName);
				folderToDelete.delete(false, true, null);

				responseComment = "Folder " + folderName + " deleted in project " + projectName;

				response = new NormalResponseMessage(requestID, 200, responseComment);

			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error processing this request. Check the console log for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}

		} else {
			//project does not exist
			responseComment = "Project does not exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);
	}

	// The class name needs to have .java at the end. Empty string for package name specifies the default package
	/**
	 * Invoking this method creates a class under a given package and project name.If the package name is empty 
	 * (e.g. Empty string), then one will be created in the default package. This method assumes the supplied
	 * project and package exists
	 * @param projectName 
	 * @param packageName
	 * @param className
	 * @return JSON formatted response indicating success of client request
	 * @throws Exception
	 * 
	 * - Tested
	 */
	private String createClass(String requestID, String projectName, String packageName, String className) throws Exception {

		NormalResponseMessage response;
		String responseComment;

		// Get the project handler
		IProject project = root.getProject(projectName);

		if (!className.contains(".java")) {
			className = className + ".java";
		}


		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);
		if (javaProject.exists()) {

			try {
				IFolder sourceFolder = project.getFolder("src");
				IPackageFragment pack = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(packageName);

				StringBuffer buffer = new StringBuffer();

				if (!packageName.isEmpty()) {
					buffer.append("package " + pack.getElementName() + ";\n");
					buffer.append("\n");
				}

				IResource resource = pack.createCompilationUnit(className, buffer.toString(), false, null).getUnderlyingResource();

				if (resource.getType() == IResource.FILE) {
					IFile ifile = (IFile) resource;
					String path = ifile.getRawLocation().toString();

					responseComment = "Class " + className + " created in package " + packageName + " in project " + projectName;
					response = new NormalResponseMessage(requestID, 200, path, responseComment);
				} else {
					responseComment = "Unknown error creating class";
					response = new NormalResponseMessage(requestID, 400, responseComment);
				}
			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error processing this request. Check the console log for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}

		} else {
			//project does not exist
			responseComment = "Project does not exist";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);

	}

	/**
	 * Helper method
	 * -Tested
	 */
	private String createPackage(String requestID, String packageName, String projectName) throws Exception {

		NormalResponseMessage response;
		String responseComment;

		// Get the project handler
		IProject project = root.getProject(projectName);
		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if (javaProject.exists()) {
			IFolder sourceFolder = project.getFolder("src");

			IPackageFragment pack = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(packageName);

			if (pack.exists()) {
				responseComment = "Package already exists";
				return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));
			}

			try {
				// create the package
				javaProject.getPackageFragmentRoot(sourceFolder).createPackageFragment(packageName, false, null);
				IPackageFragment newPackage = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(packageName);
				String packagePath = root.getRawLocation().toString() + newPackage.getPath();

				responseComment = "Package " + packageName + " created in project " + projectName;
				response = new NormalResponseMessage(requestID, 200, packagePath, responseComment);
			} catch (Exception e) {
				responseComment = "Error creating package. Try a different package name";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}

		} else {
			responseComment = "Error creating package. Project does not exist, or is invalid";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);

	}

	/**
	 * Invoking this method will delete the specified package for a given project. This method will assume that 
	 * there are no files currently in that package, otherwise it will return an error, and not perform the operation. 
	 * Move all classes into other packages before deleting this package.
	 * @param packageName
	 * @param projectName
	 * @return JSON formatted response indicating success of the request
	 * @throws Exception
	 * 
	 * - Tested
	 */
	private String deletePackage(String requestID, String packageName, String projectName) throws Exception {

		NormalResponseMessage response;
		String responseComment;
		String[] arr = packageName.split("\\.");

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		// Get the project handler
		IProject project = root.getProject(projectName);
		IJavaProject javaProject = JavaCore.create(project);
		project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

		if (javaProject.exists()) {
			try {
				IFolder sourceFolder = project.getFolder("src");
				IPackageFragment pack = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(packageName);

				if (pack.containsJavaResources() || pack.hasSubpackages()) {
					//error
					responseComment = "Error deleting package. Package is not empty";
					response = new NormalResponseMessage(requestID, 400, responseComment);
					return gson.toJson(response);
				}

				int length = arr.length;

				// The delete method for packages only deletes the very last child folder. So need to recursively delete
				// until the folders are not empty.
				while (length > 0) {

					String temp = "";

					if (length == 1) {
						temp = arr[0];
					} else {

						for (int i=0; i < length -1; i++) {
							temp += arr[i] + ".";
						}

						temp += arr[length-1];
					}


					pack = javaProject.getPackageFragmentRoot(sourceFolder).getPackageFragment(temp);

					if (pack.hasSubpackages() || pack.containsJavaResources()) {
						break;
					} else {
						pack.delete(false, null);
						System.out.println("Package " + pack.getElementName() + " deleted.");

						length --;
					}
				}

				// Package deleted successfully.
				responseComment = "Package " + packageName + " deleted";
				response = new NormalResponseMessage(requestID, 200, responseComment);


			} catch (Exception e) {
				e.printStackTrace();
				responseComment = "Unknown error deleting package, check server console for stack trace.";
				response = new NormalResponseMessage(requestID, 400, responseComment);
			}


		} else {
			responseComment = "Error creating package. Project does not exist, or is invalid";
			response = new NormalResponseMessage(requestID, 400, responseComment);
		}

		return gson.toJson(response);

	}

	/**
	 * Severity errors: 0 - info only
	 * 					1- warning
	 * 					2- error state
	 * @param requestID
	 * @param projectName
	 * @return
	 * @throws CoreException
	 * @throws BadLocationException 
	 */
	private String buildProject(String requestID, String projectName) throws CoreException, BadLocationException {

		// Get the project handler
		IProject project = root.getProject(projectName);
		ArrayList <CompilationError> compilationErrs = new ArrayList<CompilationError>();
		CompilationResponseMessage crm;

		try {

			project.open(null);
			project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);


			boolean hasNatureEnabled = project.isNatureEnabled(JavaCore.NATURE_ID);
			//System.out.println("Has nature enabled" + project.isNatureEnabled(JavaCore.NATURE_ID));

			if (!hasNatureEnabled) {
				IProjectDescription description = project.getDescription();
				description.setNatureIds(new String[] {
						JavaCore.NATURE_ID});	
				//users must call IProject.setDescription(IProjectDescription, int, IProgressMonitor) before changes to this descrption take effect
				project.setDescription(description, progressMonitor);	
				//System.out.println("Enabled nature");
			}

			project.build(IncrementalProjectBuilder.FULL_BUILD, progressMonitor);

			IMarker[] markers = project.findMarkers(null,
					true, IResource.DEPTH_INFINITE);

			//TODO: Fix up the column number for tab spaces
			for (IMarker marker: markers) {

				ICompilationUnit compilationUnit = (ICompilationUnit) JavaCore.create(marker.getResource());

				int lineNumber = Integer.parseInt(marker.getAttribute(IMarker.LINE_NUMBER).toString()) - 1;
				int offset = new Document(compilationUnit.getSource()).getLineOffset(lineNumber);
				int startPos = Integer.parseInt(marker.getAttribute(IMarker.CHAR_START).toString()) - offset;
				int endPos = Integer.parseInt(marker.getAttribute(IMarker.CHAR_END).toString()) - offset;
				String fileName = marker.getResource().getName();
			    String pathName = marker.getResource().getProjectRelativePath().toString();
			
				String errorMessage = marker.getAttribute(IMarker.MESSAGE).toString();
				errorMessage = errorMessage.replaceAll("\"", "'");
				System.out.println(errorMessage);
				String severity;

				switch (Integer.parseInt(marker.getAttribute(IMarker.SEVERITY).toString())) {
				case 0:
					severity = "info";
					break;
				case 1:
					severity = "warning";
					break;
				case 2:
					severity = "error";
					break;
				default:
					severity = "unknown";
				}


				// logging
				System.out.println("File: " + fileName + " Line number: " +  lineNumber + " At positions: " + startPos 
						+ "-" + endPos + " Message: " + marker.getAttribute(IMarker.MESSAGE) + " Severity: " + severity);

				CompilationError compError = new CompilationError(pathName, lineNumber, startPos, endPos, errorMessage, severity);
				compilationErrs.add(compError);

			}

			CompilationError[] compErrArray = new CompilationError[compilationErrs.size()];
			compilationErrs.toArray(compErrArray);

			String compilationResponse = "Project compiled successfully, " + markers.length + " errors present";

			crm = new CompilationResponseMessage(requestID, 200, 
					compilationResponse, compErrArray);

			// NEED TO SAVE!!
			ResourcesPlugin.getWorkspace().save(true, null);

			return gson.toJson(crm);

		} catch (Exception e) {
			String responseComment = "Error building project.";
			e.printStackTrace();
			NormalResponseMessage response = new NormalResponseMessage(requestID, 200, responseComment);
			return gson.toJson(response);
		}

	}

	/**
	 * Helper method for retrieving the project structure
	 * @param requestID - belonging to client
	 * @param projectName - The name of the project to retrieve the structure from
	 * @return
	 */
	private String retrieveProjectStructure(String requestID, String projectName) {

		// Get the project handler
		IProject project = root.getProject(projectName);

		try {
			project.open(null);
			project.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);

			ProjectObject po = processContainer(project, true).get(0);

			if (po != null) {
				ProjectStructureMessage psm = new ProjectStructureMessage(requestID, 200, po);
				return gson.toJson(psm);
			}

			String responseComment = "Error processing request - read console log for stack trace";
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));

		} catch (Exception e) {
			String responseComment = "Error processing request - read console log for stack trace";
			e.printStackTrace();
			return gson.toJson(new NormalResponseMessage(requestID, 400, responseComment));
		}
	}

	/**
	 * This method returns the structure of the project recursively.
	 * @param container - The root directory to start from (project)
	 * @param isRoot - Return a single item if it belongs to the calling method
	 * @return JSON formatted structure of a given project in the workspace
	 */
	private List<ProjectObject> processContainer(IContainer container, boolean isRoot) {

		try {
			if (container instanceof IContainer) {
				IResource[] members = container.members();
				ProjectObject structure = new ProjectObject();
				structure.setName(container.getName());
				structure.setPath(container.getFullPath().toString());
				structure.setType("Folder");

				List<ProjectObject> children = new ArrayList<ProjectObject>();

				for (IResource member : members) {
					ProjectObject child = new ProjectObject();
					child.setName(member.getName());	
					String path = root.getRawLocation().toString() + member.getFullPath().toString();
					child.setPath(path);

					// Classify the type of file
					if (member instanceof IContainer) {
						child.setType("Folder");
					} else if (member.getFileExtension().equals("java")) {
						child.setType("Class");
					} else if (member.getName().startsWith(".")) {
						child.setType("Metadata");
					} else {
						child.setType("File");
					}

					if (member instanceof IContainer) {			
						child.setChildren(processContainer((IContainer)member, false));
					}

					children.add(child);

				}

				structure.setChildren(children);

				if (!isRoot) {
					// return list of children to recursively called method
					return children;
				}

				// return single project object back to original calling method
				return Collections.singletonList(structure);
			}

			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		// nothing to do
	}





	private class ClientRequest implements Runnable {

		private final LinkedBlockingQueue<String> messageQueue;

		ClientRequest(LinkedBlockingQueue<String> inboundMessageQueue) {
			this.messageQueue = inboundMessageQueue;
		}

		public void run() {
			try {
				while (!Thread.currentThread().isInterrupted()) {
					String message = messageQueue.take();
					processMessage(message);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
