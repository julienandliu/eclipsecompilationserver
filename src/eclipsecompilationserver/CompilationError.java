package eclipsecompilationserver;

public class CompilationError {

	private String fileID;
	private int row;
	private int begin;
	private int end;
	private String text;
	private String type;
	private int column;
	private String raw;
	
	 /* Severity errors: 0 - info only
	 * 					 1 - warning
	 * 					 2 - error state
	 */
	public CompilationError(String fileID, int lineNumber, int charPosStart, int charPosEnd, String compErrorMsg, String severity) {
		this.fileID = fileID;
		this.row = lineNumber;
		this.begin = charPosStart;
		this.end = charPosEnd;
		this.text = compErrorMsg;
		this.type = severity;
		this.column = 0;
		this.raw = compErrorMsg;
	}
	
	public String getFileID() {
		return fileID;
	}
	
	public int getErrLineNum() {
		return row;
	}
	
	public int getErrCharPosStart() {
		return begin;
	}
	
	public int getErrCharPosEnd() {
		return end;
	}
	
	public String getCompilationErrMessage() {
		return text;
	}
	
	public String getSeverity() {
		return type;
	}
	
}
